$ErrorActionPreference = "Continue"
<# 
.NOTES 
=========================================================================== 
Step A    https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_requires?view=powershell-5.1&redirectedfrom=MSDN

Step C    https://stackoverflow.com/questions/17329443/creating-a-folder-if-it-does-not-exists-item-already-exists

Step 3    https://support.microsoft.com/en-au/help/2416434/how-to-enable-or-disable-pop3-imap-mapi-outlook-web-app-or-exchange-ac

Step 4    https://docs.microsoft.com/en-us/powershell/module/exchange/devices/get-mobiledevicestatistics?view=exchange-ps
          https://gallery.technet.microsoft.com/office/Office-365-Mobile-Device-1523900f

Step 5    https://www.experts-exchange.com/questions/27941144/powershell-to-remove-a-user-from-all-the-distribution-groups.html
          http://www.nerdybynature.com/2012/10/09/list-distribution-groups-a-user-is-a-member-of/ 
          https://www.geekshangout.com/office365-powershell-get-list-users-email-group-membership/
          https://community.spiceworks.com/scripts/show/2533-remove-user-from-all-groups-office365
          https://stackoverflow.com/questions/21534054/remove-users-from-distribution-groups-without-additional-confirmation

Step 7    https://docs.microsoft.com/en-us/office365/enterprise/powershell/remove-licenses-from-user-accounts-with-office-365-powershell
             
Step 9    https://community.spiceworks.com/topic/1982283-office-365-remove-all-licenses-from-a-user
          https://support.hdeone.com/hc/en-us/articles/115003551234-How-to-disable-Modern-Authentication-for-Office-365-applications-

Step 10    https://www.experts-exchange.com/questions/28483078/How-do-I-add-multiple-users-to-a-Powershell-command-so-that-they-have-access-to-a-calendar-on-a-mailbox.html

Step 12   https://www.experts-exchange.com/questions/29020817/Powershell-command-to-find-2FA-status-on-Office365.html
          https://stackoverflow.com/questions/48376971/how-to-execute-multi-factor-auth-from-powershell

Arrays    https://www.fpweb.net/sharepoint-blog/powershell-tips-working-with-csv-files/
=========================================================================== 
Version 6.1
Mitchell McRae
20191003
=========================================================================== 
Powershell modules required.
    MSOnline
    AzureAD
===========================================================================
.DESCRIPTION 
This script provides a standard off-boarding method for staff leaving the company. 
  
The script does the following: 
A.      Check for module: MSonline, AzureAD.
B.      
C.      Check for C:\Temp - It will create if not there.
D.      Create inputs1.csv.
E.      Check for inputs1.csv.
W1.     Get Creds (Non MFA).
Y1.     Connect to Office 365 (Non MFA).
W2      Get Creds (MFA).
Y2      Connect to Office 365 (MFA).
Z       Select connection type.

01.     Account effected.
02.     Check that account exists in Office 365 and outputs errors.
03.     Disables OWA, OWA For Devices, POP, IMAP, activesync, MAPI. - This has been disabled due to issues granting full mailbox access to other users.
04.     Removed Mobile devices from Mailbox access.
05.     Remove user from distrobution groups.
06.     Coverts user mailbox to shared mailbox.
07.     Remove Office 365 License(s).
08.     Hides the mail account from the GAL, off by default.
09.     Remove Device Security Tokens.
10.     Grant full mailbox rights.
11.     Outlook Out of Office, off by default.
12.     Disbale the Multi Factor Auth or MFA.

End 1.  Closes the current session.
End 2.  Rename inputs1.csv.   
#>

#**************** Step A. Check for Modules ****************#
#Requires -Modules MSOnline, AzureAD

#**************** Step C. Check for C:\Temp. ****************#
write-host "Step C. Check for c:\temp." -ForegroundColor Yellow
function CheckForTemp {
    $FolderPath = "C:\Temp"
    if(!(Test-Path -Path $FolderPath )){
        write-host "Creating folder C:\Temp" -ForegroundColor Green
        New-Item -Path C:\Temp -ItemType directory
        write-host ""
    }else{
        write-host "C:\Temp already exists." -ForegroundColor Green
        write-host ""
    }
}
CheckForTemp

#**************** Step D. Create Inputs1.csv. ****************#
function CreateInputs1 {
    write-host "Step D. Create Inputs1.csv." -ForegroundColor Yellow
    $createinputs1 = Read-Host "Do you want to create inputs1.csv (y/n)"
    write-host ""

    If ($createinputs1 -eq "y"){
        [pscustomobject]@{user_email = ""; full_mailbox_rights = ""; OOO_External_Message = ""; OOO_Internal_Message = ""; Hide_From_GAL = ""} | Export-Csv -Path  C:\temp\inputs1.csv -Append -NoTypeInformation
        write-host "Created inputs1.csv"
        write-host "
        **Inputs1.csv**

        username: The users full email address e.g. abc@xyz.com
        
        The below fields can be left blank.
        full_mailbox_rights: The user(s) that you want to have full access rights to this mailbox, seperate them like you would in an email.
        
        OOO_External_Message: The Outlook Out of Office message that you want external parties to get.
        
        OOO_Internal_Message: The Outlook Out of Office message that you want internal parties to get.
        
        Hide_From_GAL: y/n if you want to hide the user from the GAL, it can also be left blank."
        write-host ""
        write-host "Inputs1.csv has been created in C:\Temp, please update the csv and start again." -ForegroundColor Green
        write-host ""
        Read-Host "Press any key to exit"
        Exit

    }elseif ($createinputs1 -eq "n"){
        write-host "Please place inputs1.csv into C:\Temp and start this script again."  -ForegroundColor Yellow
        write-host ""
        Read-Host "Press any key to exit"
        Exit
    
    }else{
        write-host "**********************************************" -ForegroundColor Red
        write-host "Please enter y/n" -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        CreateInputs1
    }
}

#**************** Step E. Check inputs1.csv. ****************#
function CheckInputs1 {
    write-host "Step E. Check inputs1.csv." -ForegroundColor Yellow
    
    $path = "C:\Temp\inputs1.csv"
    if(!(Test-Path -Path $path )){
        write-host "**********************************************" -ForegroundColor Red
        write-host "The Inputs1.csv file doesn't exist in C:\Temp." -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        CreateInputs1
              
    }else{
        write-host "Inputs1.csv exists" -ForegroundColor Green
        write-host ""
    }
}
CheckInputs1
#**************** Step F. Check inputs1.csv. ****************#
function CheckIfInputs1IsOpen {
    write-host "Step F. Check If inputs1 Is Open." -ForegroundColor Yellow
       function TestFileLock ($FilePath ){
        $FileLocked = $false
        $FileInfo = New-Object System.IO.FileInfo $FilePath
        trap {Set-Variable -name Filelocked -value $true -scope 1; continue}
        $FileStream = $FileInfo.Open( [System.IO.FileMode]::OpenOrCreate, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None )
        if ($FileStream) {$FileStream.Close()}
        $FileLocked
    }
    $File = "C:\Temp\inputs1.csv"
    if (TestFileLock $File) {Write-Output "File '$File' is locked"} else {Write-Output "File '$File' is not locked"}
}
CheckIfInputs1IsOpen

#/////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\#

#**************** Step W1. Get Creds. ****************#
function GetCreds {
    write-host "Step Y. Get Creds." -ForegroundColor Yellow

    write-host "Importing Office 365 PowerShell Commandlets" 
    Write-Host -ForegroundColor White -BackgroundColor DarkBlue "*** PLEASE ENTER OFFICE 365 ADMIN CREDENTIALS ***" 
        
    #The below stores the admin username in a plain text file.
    Read-Host "Enter Username" | Out-File "C:\Temp\Username.txt"
    #The below stores the admin password in an encrypted text file, only decryptable to the UID it is run on.
    Read-Host "Enter Password" -AsSecureString |  ConvertFrom-SecureString | Out-File "C:\Temp\Password.txt"
}


#**************** Step Y1. Connects to Office 365. ****************#
function connect_o365 {
    #Pulls the stored creds and uses them to login to Office 365.
    $AdminUsername = Get-Content C:\Temp\Username.txt
    $EncryptedPass = Get-Content C:\Temp\Password.txt | ConvertTo-SecureString
    $credential = New-Object System.Management.Automation.PsCredential($AdminUsername, $EncryptedPass)

    #Connects to Office 365.
    Connect-MsolService -Credential $credential
    $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://ps.outlook.com/powershell/" -Credential $credential -Authentication Basic -AllowRedirection
    Import-PSSession $Session
    Connect-AzureAD -Credential $credential
}



#**************** Step W2. Get Creds for MFA connection. ****************#
function GetCredsMFA {
    write-host "Step Y. Get Creds." -ForegroundColor Yellow

    write-host "Importing Office 365 PowerShell Commandlets" 
    Write-Host -ForegroundColor White -BackgroundColor DarkBlue "*** PLEASE ENTER OFFICE 365 ADMIN CREDENTIALS ***" 
        
    #The below stores the admin username in a plain text file.
    Read-Host "Enter Username" | Out-File "C:\Temp\Username.txt"
    
    <#The below doesn't work with 2FA/MFA sesssions.
    The below stores the admin password in an encrypted text file, only decryptable to the UID it is run on.
    Read-Host "Enter Password" -AsSecureString |  ConvertFrom-SecureString | Out-File "C:\Temp\Password.txt"#>
}


#**************** Step Y2. Connects to Office 365 for MFA connection. ****************#
function connect_o365MFA {
    
    $AdminUsername = Get-Content C:\Temp\Username.txt
    
    $PSExoPowershellModuleRoot = (Get-ChildItem -Path $env:userprofile -Filter CreateExoPSSession.ps1 -Recurse -ErrorAction SilentlyContinue -Force | Select -Last 1).DirectoryName
    $ExoPowershellModule = "Microsoft.Exchange.Management.ExoPowershellModule.dll";
    $ModulePath = [System.IO.Path]::Combine($PSExoPowershellModuleRoot, $ExoPowershellModule);

    Import-Module $ModulePath;

    $Office365PSSession = New-ExoPSSession -UserPrincipalName $AdminUsername -ConnectionUri "https://outlook.office365.com/powershell-liveid/"
    write-host "This will take a couple of minutes." -ForegroundColor Yellow
    
    Import-PSSession $Office365PSSession
    Connect-AzureAD -AccountId $AdminUsername #-AccountId seems to pass through the session token to the Azure login.
    
}

#**************** Step Z. Seclect connection type. ****************#
function ConnectionType {
    write-host "Step Z. Select a connection type."
    write-host "1. Non MFA"
    write-host "2. MFA"
    $ConnectionOption = Read-Host "Please select the connection type:"

    if ($ConnectionOption -eq 1) {
        GetCreds
        connect_o365

    }elseif ($ConnectionOption -eq 2) {
        GetCredsMFA
        connect_o365MFA

    }else{
        write-host "**********************************************" -ForegroundColor Red
        write-host "Please select one of the above options" -ForegroundColor Gray
        write-host "**********************************************" -ForegroundColor Red
        write-host ""
        Start-Sleep -Seconds 2
        ConnectionType
    }
}
ConnectionType

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

#**************** Step 1. Account effected. ****************#
function AccountEffected {
    param([string]$username)
    #To make it easyer to notice when a new mailbox has started.
    write-host "-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-" -ForegroundColor Magenta
    write-host "-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-" -ForegroundColor Magenta
    write-host "-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-" -ForegroundColor Magenta
    write-host ""
    Write-Host "Account being terminated: $username" -ForegroundColor Yellow
}

#**************** Step 2. Check that account exists in Office 365. ****************#
function CheckMailbox {
    param([string]$username)
    
    $CurrentDate = Get-Date
    $CurrentDate = $CurrentDate.ToString('yyyyMMdd HHmm')

    If (Get-Mailbox -Identity $username -ErrorAction SilentlyContinue) {
    write-host "Mailbox $username exists" -ForegroundColor Green
    }Else{
    write-host "**********************************************" -ForegroundColor Red
    write-host "Mailbox $username doesn't exist, waiting 5 seconds."
    write-host ""
    write-host "Updated Termination Failures csv in C:\Temp"
    write-host "**********************************************" -ForegroundColor Red
    [pscustomobject]@{ Username = "$username"} | Export-Csv -Path  C:\temp\"Termination Failures $CurrentDate".csv -Append -NoTypeInformation
    Start-Sleep -s 5
    continue
    }   
}

#**************** Step 3. Disabling OWA, OWA For Devices, POP, IMAP, activesync and MAPI access for user. ****************#
<# This has been disabled due to issues granting full mailbox rights to users.
function DisableOwa {
    param([string]$username)
    write-host ""
    Write-Host "Step 3. Disabling OWA, OWA For Devices, POP, IMAP, activesync, MAPI access for user." -ForegroundColor DarkCyan 
    Write-Host "If you get error: The operation couldn't be performed because object couldn't be found on server. User has no license." -ForegroundColor DarkYellow 
    Set-CASMailbox -Identity $username -OWAEnabled $false -POPEnabled $false -ImapEnabled $false -ActiveSyncEnabled $false -OWAforDevicesEnabled $false -MAPIEnabled $false
}
#>

#**************** Step 4. Remove Mobile devices from Mailbox access. ****************#
function RemoveMobileDevices {
    param([string]$username)

    write-host ""
    Write-Host "Step 4. Remove Mobile devices from Mailbox access." -ForegroundColor DarkCyan 
    Write-Host "If the device list only lists the devices once then it has worked, if it lists them twice it has failed." -ForegroundColor DarkYellow
    Write-Host "If this was skipped, then no devices were linked." -ForegroundColor Green

    #Connect-MsolService -Credential $UserCredential
    Write-Host (Get-MobileDevice -Mailbox $username | Select DeviceOS, DeviceId)

    Get-MobileDevice -Mailbox $username | Remove-MobileDevice -confirm:$false

    Write-Host (Get-MobileDevice -Mailbox $username | Select DeviceOS, DeviceId)
}

#**************** Step 5. Remove user from distrobution groups. ****************#
function RemoveDistrobutionGroups {
    param([string]$username)

    write-host ""
    write-host "Step 5. Remove mailbox from distrobution groups." -ForegroundColor DarkCyan 

    $groups = Get-DistributionGroup
    $DGs = $groups | where-object { ( Get-DistributionGroupMember $_ | where-object { $_.PrimarySmtpAddress -contains $username}) } | Format-Table | Out-String

    foreach( $dg in $DGs){
        Remove-DistributionGroupMember $dg -Member $user
    }

    $DGs = $groups | where-object { ( Get-DistributionGroupMember $_ | where-object { $_.PrimarySmtpAddress -contains $username}) } | Format-Table | Out-String
}

#**************** Step 6. Coverts user mailbox to shared mailbox. ****************# 
function ConvertToSharedMailbox {
    param([string]$username)
    
    write-host ""
    write-host "Step 6. Coverts user mailbox to shared mailbox" -ForegroundColor DarkCyan
    Set-Mailbox -Identity $username -Type shared
}

#**************** Step 7. Remove Office 365 License. ****************# 
function RemoveLicenses {
    param([string]$username)

    write-host ""
    write-host "Step 7. Remove Office 365 License" -ForegroundColor DarkCyan
    
    Get-MsolUser -UserPrincipalName $username | Format-List DisplayName, Licenses

    (get-MsolUser -UserPrincipalName $username).licenses.AccountSkuId |
        foreach {
        Set-MsolUserLicense -UserPrincipalName $username -RemoveLicenses $_
    }

    write-host ""
    write-host "Licenses {} should be empty"
    Get-MsolUser -UserPrincipalName $username | Format-List DisplayName, Licenses
}

#**************** Step 8. Hide User From The Global Address lists. ****************#
function HideFromGAL {
    param([string] $username, [string] $Hide_From_GAL, [string] $confirmation)

    write-host ""
    Write-Host "Step 8. Hide User From The Global Address lists" -ForegroundColor DarkCyan
    
    if ($Hide_From_GAL -eq "y") {
        Set-Mailbox -Identity $username -HiddenFromAddressListsEnabled $true
        Write-Host "Hidden from GAL" -ForegroundColor Red
    }
    elseif ($Hide_From_GAL -eq "n") {
        Write-Host "This was skipped" -ForegroundColor Green
    
    }
    else {
        $Hide_From_GAL = read-host "Please enter y/n"
        HideFromGAL
    }
}

#**************** Step 9. Remove Device Security Tokens. ****************#
function RemoveSecurityTokens {
    param([string]$username)

    write-host ""
    write-host "Step 9. Remove Device Security Tokens" -ForegroundColor DarkCyan
    write-host "This will be blank." -ForegroundColor Green
    
    Revoke-AzureADUserAllRefreshToken -ObjectId (Get-MsolUser -UserPrincipalName $username).objectId 
}

#**************** Step 10. Grant full mailbox rights. ****************#
function GrantFullMailboxRights {
    param([string]$username, [string]$full_mailbox_rights)

    write-host ""
    write-host "Step 10. Grant full mailbox rights." -ForegroundColor DarkCyan 

    foreach ($User in $full_mailbox_rights) {
        Add-mailboxpermission -Identity $username -User $User -AccessRights FullAccess -erroraction SilentlyContinue;
    }
}

#**************** Step 11. Outlook Out of Office. ****************#
function SetOOO {
    param([string]$username, [string]$OOO_External_Message, [string]$OOO_Internal_Message)

    write-host ""
    write-host "Step 11. Outlook Out of Office." -ForegroundColor DarkCyan 
    
    #If OOO messages are blank then no OOO will be set.
    If ([string]::IsNullOrWhiteSpace($OOO_External_Message)) {
        write-host "No external message was set."
        }else{
        Set-MailboxAutoReplyConfiguration -Identity $username -AutoReplyState enabled -ExternalAudience all -ExternalMessage $OOO_External_Message 
        write-host "External message was set."
    }

    If ([string]::IsNullOrWhiteSpace($OOO_Internal_Message)) {
        write-host "No internal message was set."
        }else{
        Set-MailboxAutoReplyConfiguration -Identity $username -AutoReplyState enabled -ExternalAudience all -InternalMessage $OOO_Internal_Message
        write-host "Internal message was set."
    }
}

#**************** Step 12. Dsiable the MFA. ****************#
<# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
The below resets all their MFA's, though this doensn't have the intended outcome. 
The user will still be able to resync with the existing token on their phone, it doesn't terminate the token on the phone.
Steps 2 and 7 also don't remove or deauthorize the existing MFA tokens from a mobile devices.
Currently (14/12/18) the only known way to remove a 2FA token off a mobile remotely, is to wipe it.

Reset-MsolStrongAuthenticationMethodByUpn -UserPrincipalName $username.UserPrincipalName
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #>

function DisableMFA {
    param([string]$username)

    write-host ""
    write-host "Step 12. Disable the MFA." -ForegroundColor DarkCyan 

    #This checks their MFA status.
    $ThisUSer = Get-msoluser -UserPrincipalName $username | Select-Object -ExpandProperty StrongAuthenticationRequirements
    if ($ThisUser -eq $null) {
        $ThisUser = "Not Enabled"
        $thisUser
    }
    else {
        $ThisUSer.state
    }

    Get-MobileDevice -Mailbox $username | Remove-ActiveSyncDevice -confirm:$false

    #This disables all their MFA's. Re enabling this will allow all their prior tokens to work again.
    Set-MsolUser -UserPrincipalName $username -StrongAuthenticationRequirements @()


    #This checks their MFA status after the changes.
    $ThisUSer = Get-msoluser -UserPrincipalName $username | Select-Object -ExpandProperty StrongAuthenticationRequirements
    if ($ThisUser -eq $null) {
        $ThisUser = "Not Enabled"
        $thisUser
    }
    else {
        $ThisUSer.state
    }
}

#**************** Step End 1. Closes the current session. ****************#
function CloseSession {
write-host ""
write-host "Step End 1. Closes the current session." -ForegroundColor DarkRed
Remove-PSSession -Session (Get-PSSession)
}

#**************** Step End 2. Rename inputs1.csv. ****************#
function RenameInputs1 {
    write-host ""
    write-host "Step End 2. Rename inputs1.csv." -ForegroundColor DarkRed
    
    $AdminUsername = Get-Content C:\Temp\Username.txt

    $confirmation = Read-Host "Do you want to reanme inputs1.csv, this prevents reuse. y/n"
    if ($confirmation -eq "y") {
        Rename-Item "c:\temp\inputs1.csv" "inputs1 $AdminUsername.csv"
        Write-Host "inputs1 has been renamed" -ForegroundColor Yellow
   }else{
        Write-Host "This was skipped" -ForegroundColor Green
   }
}


#------------------------- CSV inputs and running of functions -----------------------------------------#
$inputs = import-csv c:\temp\inputs1.csv
ForEach ($item in $inputs) {
    
    $username = $item.user_email
    $full_mailbox_rights = $item.full_mailbox_rights
    $OOO_External_Message = $item.OOO_External_Message
    $OOO_Internal_Message = $item.OOO_Internal_Message
    $Hide_From_GAL = $item.Hide_From_GAL
  
    #Runs the functions listed below and with the required variables.
    AccountEffected $username
    CheckMailbox $username
    #DisableOwa $username
    RemoveMobileDevices $username
    RemoveDistrobutionGroups $username
    ConvertToSharedMailbox $username
    RemoveLicenses $username
    HideFromGAL $username $Hide_From_GAL
    RemoveSecurityTokens $username
    GrantFullMailboxRights $username $full_mailbox_rights
    SetOOO $username $OOO_External_Message $OOO_Internal_Message
    DisableMFA $username
}

#Closes the Powershell session to Office 365, this prvents too many connections error.
CloseSession
RenameInputs1